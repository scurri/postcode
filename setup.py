import os

from setuptools import find_packages, setup


def read(fname):
    path = os.path.join(os.path.dirname(__file__), fname)
    with open(path) as f:
        return f.read()


setup(
    name='postcode',
    version='1.0.0',
    description=(
        'The LuizaLabs set of tools '
        'to ease asyncio development'
    ),
    long_description=read('README.rst'),
    author='Vitor Mantovani',
    author_email='vtrmantovani@gmail.com',
    url='https://bitbucket.org/scurri/postcode',
    keywords='uk postcode',
    install_requires=[],
    packages=find_packages(exclude=[
        'tests*'
    ]),
    classifiers=[
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3.6',
    ]
)
