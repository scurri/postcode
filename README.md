# UK Postcode #
[![CircleCI](https://circleci.com/bb/scurri/postcode.svg?style=svg)](https://circleci.com/bb/scurri/postcode)
[![codecov](https://codecov.io/bb/scurri/postcode/branch/master/graph/badge.svg)](https://codecov.io/bb/scurri/postcode)

Postcode it is a simple project make with python for formatting and validation UK postcode.

## Dependencies

- [Python 3.6](https://www.python.org/downloads/)


## Run the tests

1. Run the command:

    ```
    make test
    ```

## Usage

Function to format the postcode

Example:
```python
from postcode import format_code

format_code('ec1a1bb')
# Return EC1A 1BB
```

Function to validate the postcode.

* This function validates with the regex provided from UK government to see if the text follow the rules of postcodes from UK and use the service 'postcodes' for search on database from 'Office for National Statistics Data Portal' to know if the postcode it was deleted.

Example:
```python
from postcode import validate_code

validate_code('EC1A 1BB')
# Return True

validate_code('A 1AA')
# Return False

validate_code('AB1 1AA')
# Return False
```

## Resource:

* https://en.wikipedia.org/wiki/Postcodes_in_the_United_Kingdom#Formatting
* https://postcodes.io
