import pytest

from postcode import FormartError, format_code


class TestFormat:

    @pytest.mark.parametrize("test_input, expected", [
        ('ec1a 1bb', 'EC1A 1BB'),  # Format (AA9A 9AA)
        ('w1a0ax', 'W1A 0AX'),     # Format (A9A 9AA)
        ('M11AE', 'M1 1AE'),       # Format (A9 9AA)
        ('B338TH', 'B33 8TH'),     # Format (A99 9AA)
        ('Cr26xH', 'CR2 6XH'),     # Format (AA9 9AA)
        ('Dn551pt', 'DN55 1PT'),   # Format (AA99 9AA)
    ])
    def test_format_postcode(self, test_input, expected):
        assert format_code(test_input) == expected

    @pytest.mark.parametrize("test_input, length", {
        ('A99A', 4),        # Length less than 6
        ('DN55 1PTNN', 10),  # Length large than 8
    })
    def test_format_postcode_with_wrong_length(self, test_input, length):
        with pytest.raises(FormartError) as e:
            format_code(test_input)

        assert str(e.value) == "Code length '{}' is invalid".format(length)
