import pytest

from postcode import validate_code


class TestValidate:

    @pytest.mark.parametrize("test_input, expected", [
        ('EC1A 1BB', True),    # Format (AA9A 9AA)
        ('W1T 1HX', True),     # Format (A9A 9AA)
        ('M1 1AE', True),      # Format (A9 9AA)
        ('B33 8TH', True),     # Format (A99 9AA)
        ('CR2 6XH', True),     # Format (AA9 9AA)
        ('DN55 1PT', True),    # Format (AA99 9AA)
        ('DI55 1PT', False),   # Bad letter in 2nd position
        ('A11A 1AA', False),   # Invalid digits in 1st group
        ('AA 1AA', False),     # No digit in 1st group
        ('A 1AA', False),      # No digit in 1st group
        ('AA11A 1AA', False),  # 1st group too long
        ('AA11 1AAA', False),  # 2nd group too long
        ('1A 1AA', False),     # Missing letter in 1st group
        ('11 1AA', False),     # Missing letter in 1st group
        ('AA1 1', False),      # Missing letter in 2nd group
        ('AB1 1AA', False),    # Valid Format but deleted postcode
    ])
    def test_validate_postcode(self, test_input, expected):
        assert validate_code(test_input) == expected
