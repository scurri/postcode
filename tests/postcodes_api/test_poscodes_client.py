from unittest.mock import patch

import pytest
from requests.exceptions import RequestException

from postcode.postcodes_api import PostCodesApiClient, PostCodesApiClientError
from tests.postcodes_api.base import BaseTestCase


class TestPostCodesApiClient(BaseTestCase):

    def test_validate_postcode(self):
        with self.vcr.use_cassette(
            'test_validate_postcode.json'
        ):
            postcodes_client = PostCodesApiClient()
            result = postcodes_client.validate_postcode("EC1A 1BB")
            assert result

    def test_validate_postcode_invalid_postcode(self):
        with self.vcr.use_cassette(
            'test_validate_postcode_invalid_postcode.json'
        ):
            postcodes_client = PostCodesApiClient()
            result = postcodes_client.validate_postcode("AB1 1AA")
            assert not result

    @patch('postcode.postcodes_api.requests.get')
    def test_validate_with_requests_exception(self, my_request_mock):
        my_request_mock.request.side_effect = RequestException

        with pytest.raises(PostCodesApiClientError) as e:
            postcodes_client = PostCodesApiClient()
            postcodes_client.validate_postcode("AB1 1AA")

        assert str(e.value) == "Error parsing request data"

    def test_validate_postcode_request_return_error_code(self):
        with self.vcr.use_cassette(
            'test_validate_postcode_request_return_error_code.json'
        ):
            with pytest.raises(PostCodesApiClientError) as e:
                postcodes_client = PostCodesApiClient()
                postcodes_client.validate_postcode("AB1 1AA")

            assert str(e.value) == "Error parsing request data"

    def test_validate_postcode_error_decode_json(self):
        with self.vcr.use_cassette(
            'test_validate_postcode_error_decode_json.json'
        ):
            with pytest.raises(PostCodesApiClientError) as e:
                postcodes_client = PostCodesApiClient()
                postcodes_client.validate_postcode("EC1A 1BB")

            assert str(e.value) == "Error transforming text into json"

    def test_validate_postcode_with_wrong_payload(self):
        with self.vcr.use_cassette(
            'test_validate_postcode_with_wrong_payload.json'
        ):
            with pytest.raises(PostCodesApiClientError) as e:
                postcodes_client = PostCodesApiClient()
                postcodes_client.validate_postcode("EC1A 1BB")

            assert str(e.value) == "PostCodesApi don't return" \
                                   " status and result on payload"

    def test_validate_postcode_return_error_code(self):
        with self.vcr.use_cassette(
            'test_validate_postcode_return_error_code.json'
        ):
            with pytest.raises(PostCodesApiClientError) as e:
                postcodes_client = PostCodesApiClient()
                postcodes_client.validate_postcode("EC1A 1BB")

            assert str(e.value) == "PostCodesApi return error code"
