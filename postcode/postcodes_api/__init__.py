import json
import logging

import requests

BASE_URL = "https://api.postcodes.io/postcodes/"

_logger = logging.getLogger('postcodes_api.post_code_api_client')


class PostCodesApiClientError(Exception):
    pass


class PostCodesApiClient(object):

    def __init__(self, host=BASE_URL):
        self.host = host

    def validate_postcode(self, postcode):
        try:
            url = self.host + postcode + "/validate"
            _logger.info("Request: {}".format(url))
            response = requests.get(url, timeout=10)

            if response.status_code != 200:
                _logger.error("Error parsing request data")
                raise PostCodesApiClientError("Error parsing request data")

            payload = json.loads(response.text)

            status = payload['status']
            result = payload['result']

            if status != 200:
                _logger.error("PostCodesApi return error code")
                raise PostCodesApiClientError("PostCodesApi return error code")

            return bool(result)

        except PostCodesApiClientError as e:
            raise PostCodesApiClientError(e)

        except requests.exceptions.RequestException:
            _logger.error("Error parsing request data")
            raise PostCodesApiClientError("Error parsing request data")
        except KeyError:
            _logger.error("PostCodesApi don't return status"
                          " and result on payload")
            raise PostCodesApiClientError("PostCodesApi don't return status"
                                          " and result on payload")

        except json.decoder.JSONDecodeError as e:
            _logger.error("JSONDecodeError: {}".format(e))
            raise PostCodesApiClientError("Error transforming text into json")
