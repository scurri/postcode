import re
from postcode.postcodes_api import PostCodesApiClient, PostCodesApiClientError

REGEX = '^([Gg][Ii][Rr] 0[Aa]{2})|((([A-Za-z][0-9]{1,2})|' \
        '(([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})|' \
        '(([A-Za-z][0-9][A-Za-z])|' \
        '([A-Za-z][A-Ha-hJ-Yj-y][0-9]?[A-Za-z]))))' \
        ' {0,1}[0-9][A-Za-z]{2})$'


class FormartError(Exception):
    pass


def validate_code(postcode):
    try:
        regex_validation = re.compile('{}'.format(REGEX)).match(postcode)

        if not regex_validation:
            return False

        postcodes_client = PostCodesApiClient()

        return postcodes_client.validate_postcode(postcode)
    except PostCodesApiClientError as e:
        raise PostCodesApiClientError(e)


def format_code(code):
    try:
        if len(code) < 5 or len(code) > 9:
            raise FormartError("Code length '{}' is invalid".format(len(code)))

        code = code.upper()
        out_code = code[:-3].strip()
        inw_code = code[-3:].strip()
        return '{} {}'.format(out_code, inw_code)
    except FormartError as e:
        raise FormartError(e)
